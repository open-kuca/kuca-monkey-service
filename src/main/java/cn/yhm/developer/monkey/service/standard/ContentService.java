package cn.yhm.developer.monkey.service.standard;

import cn.yhm.developer.monkey.model.entity.ContentEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * Content表服务接口
 *
 * @author victor2015yhm@gmail.com
 * @since 2023-05-21 17:54:32
 */
public interface ContentService extends IService<ContentEntity> {
}
