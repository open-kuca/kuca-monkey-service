package cn.yhm.developer.monkey.model.request;

import cn.yhm.developer.kuca.ecology.model.request.EcologyRequest;

/**
 * 健康检查请求参数
 *
 * @author victor2015yhm@gmail.com
 * @since 2023-05-02 14:12:16
 */
public class HealthCheckRequest implements EcologyRequest {
}
