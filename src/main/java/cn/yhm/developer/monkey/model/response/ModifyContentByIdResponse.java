package cn.yhm.developer.monkey.model.response;

import cn.yhm.developer.kuca.ecology.model.response.EcologyResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * 修改内容响应参数
 *
 * @author victor2015yhm@gmail.com
 * @since 2023-05-07 16:22:42
 */
@Getter
@Setter
public class ModifyContentByIdResponse implements EcologyResponse {

}
