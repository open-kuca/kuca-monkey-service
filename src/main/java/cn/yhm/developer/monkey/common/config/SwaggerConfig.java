package cn.yhm.developer.monkey.common.config;

import org.springframework.context.annotation.Configuration;

/**
 * Swagger配置类
 *
 * @author victor2015yhm@gmail.com
 * @since 2023-05-22 07:52:39
 */
@Configuration
public class SwaggerConfig {
}
